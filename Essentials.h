//
//  Essentials.h
//  Essentials
//
//  Created by Martin Kiss on 7.6.13.
//  Copyright (c) 2013 iAdverti. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Foundation+Essentials.h"

#if TARGET_OS_IPHONE
    #import <UIKit/UIKit.h>
    #import "UIKit+Essentials.h"
#endif

#import <CoreImage/CoreImage.h>
#import "CoreImage+Essentials.h"
